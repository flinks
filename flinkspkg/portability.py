import sys
from locale import getpreferredencoding

# addstrwrapped: wrapper for curses' addstr which gets around compatibility
# issues between python 2.x and python 3.x.
# s should be a unicode string in 2.x, or a string in 3.x
def addstrwrapped(scr, y, x, s, attr=0):
    if sys.hexversion > 0x03000000:
        if y is None:
            return scr.addstr(s, attr)
        return scr.addstr(y, x, s, attr)
    else:
        if y is None:
            return scr.addstr(s.encode(getpreferredencoding(), "replace"), attr)
        return scr.addstr(y, x, s.encode(getpreferredencoding(), "replace"), attr)

# I want cmp back.
def _cmp(x,y):
    if sys.hexversion > 0x03000000:
        return (x>y)-(y>x)
    else:
        return cmp(x,y)

if sys.hexversion > 0x03000000:
    from curses import wrapper
    curseswrapper = wrapper
else:
    from curses.wrapper import wrapper
    curseswrapper = wrapper
