# Part of flinks
# (C) Martin Bays 2008
# Released under the terms of the GPLv3

from __future__ import unicode_literals

import sys, os
from string import *

from time import sleep, time
import re

import signal

import curses
#from curses.textpad import Textbox
from .TextboxPad import TextboxPad
import curses.ascii as ascii

from .IndexedHypertext import IndexedHypertext
from .portability import addstrwrapped, _cmp

class QuitException(Exception):
    pass

class Browser:
    def __init__(self, scr, config):
        self.settings = config
        self.scr = scr

        self.setGeom()

        curses.use_default_colors()
        for i in range(1,8):
            curses.init_pair(i, i, -1)
        curses.init_pair(8, curses.COLOR_BLACK, -1)

        self.scr.timeout(0)

        self.scr.leaveok(1)
        curses.curs_set(0)

        self.paused = True

        self.lastSearch = None

        self.itext = None

        self.cachedDocuments = []

        # history *ends* with most recent url
        # unHistory *ends* with *current* url
        self.history = []
        self.unHistory = []

        self.tricky = [""]*self.trickyDisplayNum

        self.counting = False
        self.count = 0

        self.skim = False
        self.normalWpm = self.wpm

        self.interruptible = False

        self.urlHistory = []
        self.commandHistory = []
        self.searchHistory = []

        self.speechsub = None
        self.spokeTo = -1

        self.globalMarks = self.readMarksFile()

    def readMarksFile(self):
        try:
            marksFile = open(os.getenv("HOME")+"/.flinksMarks", 'r')
        except IOError:
            return {}
        marks = {}
        while True:
            m = re.match('(.)\s*:?\s*', marksFile.readline())
            if not m: break
            mark = m.groups()[0]
            url = marksFile.readline().strip()
            try: point = int(marksFile.readline().strip())
            except: break
            marks[mark] = [url, point]
        return marks

    def writeMarksFile(self, marks):
        marksFile = open(os.getenv("HOME")+"/.flinksMarks", 'w')
        for (mark,(url,point)) in marks.items():
            marksFile.write('%s:\n\t%s\n\t%d\n' % (mark, url, point))

    # XXX: we e.g. use self.wpm as convenient shorthand for
    # self.settings.options["wpm"], etc. The overriding below of __getattr__
    # and __setattr__ implement this.
    def __getattr__(self, name):
        if name != "settings" and name in self.settings.optionNames:
            return self.settings.options[name]
        else:
            raise AttributeError
    def __setattr__(self, name, value):
        if name != "settings" and name in self.settings.optionNames:
            self.settings.options[name] = value
        else:
            self.__dict__[name] = value

    class Geom:
        def __init__(self, scr, desiredLinkDisplayNum, desiredTrickyDisplayNum):
            (maxy, maxx) = scr.getmaxyx()

            # effective maximums, i.e. with anything bigger scr.move(y,x)
            # returns ERR:
            self.maxy = max(0, maxy-2)
            self.maxx = max(0, maxx-1)

            # we aim for following layout, and squash as necessary
            # desiredLinkDisplayNum lines of links
            #
            # wordline
            #
            # desiredTrickyDisplayNum lines of trickies
            # statusline
            # infoline
            # commandline
            squashed = self.maxy < (desiredLinkDisplayNum +
                    desiredTrickyDisplayNum + 6)

            self.wordline = self.maxy//2

            self.linkDisplayNum = min(desiredLinkDisplayNum,
                    self.wordline-1)
            self.trickyDisplayNum = min(desiredTrickyDisplayNum,
                    (self.maxy - self.wordline) - 2)

            self.linkline = self.wordline - self.linkDisplayNum - (not squashed)

            self.trickyline = self.wordline + (self.maxy > self.wordline) + (not squashed)
            self.statusline = self.trickyline + self.trickyDisplayNum
            self.infoline = self.statusline + (self.maxy > self.statusline)
            self.commandline = self.infoline + (self.maxy > self.infoline)

    def setGeom(self):
        self.geom = self.Geom(self.scr, self.linkDisplayNum,
                self.trickyDisplayNum)

    def blockGetKey(self, promptLine=None, prompt=None):
        if promptLine and prompt:
            self.displayCleanCentred(promptLine, prompt)
        self.scr.timeout(-1)
        key = self.scr.getkey()
        self.scr.timeout(0)
        if promptLine and prompt:
            self.clearLine(promptLine)
        if key == '\x1b' or key == '\x07':
            # esc or ^G: cancel
            return None
        return key
    def displayCentred(self, line, str, attr=0):
        addstrwrapped(self.scr, line,
                max(0,(self.geom.maxx-len(str))//2),
                str[:self.geom.maxx],
                attr)
    def clearLine(self, line):
        self.scr.move(line,0)
        self.scr.clrtoeol()
    def displayCleanCentred(self, line, str, attr=0):
        self.clearLine(line)
        self.displayCentred(line, str, attr)
        self.scr.refresh()
    def displayStatus(self):
        if self.geom.statusline == self.geom.wordline: return
        def getStatusLine(abbreviated=False):
            if not abbreviated:
                text = 'WPM: %d' % self.wpm
                if self.showPoint:
                    text += '; word: %d' % (self.itext.atWord+1)
                    text += '; sentence: %d' % (self.itext.sentenceIndex.at+1)
                    text += '; paragraph: %d' % (self.itext.paraIndex.at+1)
                    text += '; link: %d' % (self.itext.linkIndex.at+1)
                if self.paused: text += '; PAUSED'
            else:
                text = '%d ' % self.wpm
                if self.showPoint:
                    text += '%d/' % (self.itext.atWord+1)
                    text += '%d/' % (self.itext.sentenceIndex.at+1)
                    text += '%d/' % (self.itext.paraIndex.at+1)
                    text += '%d ' % (self.itext.linkIndex.at+1)
                if self.paused: text += 'P'
            return text

        self.clearLine(self.geom.statusline)

        line = getStatusLine(self.abbreviate)
        if len(line) >= self.geom.maxx and not self.abbreviate:
            line = getStatusLine(True)
        if len(line) >= self.geom.maxx:
            return

        self.displayCentred(self.geom.statusline, line)

    def displayLinkListEntry(self, linknum, emph=False):
        if self.geom.linkDisplayNum <= 0: return
        line = self.geom.linkline + linknum%self.geom.linkDisplayNum
        if linknum < 0:
            self.clearLine(line)
            return
        attr = self.attrOfLinknum(linknum)
        if emph:
            attr |= curses.A_BOLD
            emphstr = '*'
        else:
            emphstr = ''
        link = self.itext.links[linknum]
        str = '%s%s "%s": %s' % (
                emphstr, self.strOfLinknum(linknum),
                self.abbreviateStr(self.cleanWord(link["word"]), 10),
                self.abbreviateStr(link["url"])
                )
        self.clearLine(line)
        self.displayCentred(line, str, attr)
    def displayLinkList(self):
        for linknum in range(
                self.itext.linkIndex.at - self.geom.linkDisplayNum,
                self.itext.linkIndex.at+1):
            emphasize = (self.itext.linkIndex.at - linknum <
                    self.linkDisplayEmphNum)
            self.displayLinkListEntry(linknum, emphasize)

    def attrOfLinknum(self, linknum):
        if linknum is None or self.geom.linkDisplayNum <= 0: return 1
        c = (linknum%self.geom.linkDisplayNum)%5+1
        if c >= 4: c+=1 # don't use blue (hard to see against black)
        return curses.color_pair(c)

    def strOfLinknum(self, linknum):
        if self.geom.linkDisplayNum <= 0: return
        return '[%d]' % ((linknum%self.geom.linkDisplayNum)+1)

    def displayTrickyList(self):
        for i in range(self.geom.trickyDisplayNum):
            self.clearLine(self.geom.trickyline + i)
            self.displayCentred(
                    self.geom.trickyline + i,
                    self.tricky[i]
                    )

    def addTricky(self, word):
        if self.geom.trickyDisplayNum <= 0:
            return
        for t in self.tricky:
            if word == t:
                # already in the list; don't repeat
                return
        for i in range(self.geom.trickyDisplayNum-1, 0, -1):
            self.tricky[i] = self.tricky[i-1]
        self.tricky[0] = word
        self.displayTrickyList()

    def isTricky(self, word):
        return (
                bool(re.search('\d', word)) or bool(re.search('\w-+\w', word))
                or len(word) > 15
                )

    def cleanWord(self, word):
        for (start, end) in (('(',')'),('[',']'),('{','}')):
            # remove non-matching brackets from top and tail:
            if word[0] == start:
                if end not in word:
                    word = word[1:]
            elif word[-1] == end and start not in word:
                    word = word[:-1]
        return word.strip(' ,.;:?!')

    def abbreviateStr(self, str, maxLen = None):
        if maxLen is None: maxLen = 2*self.geom.maxx//3
        if len(str) <= maxLen: return str
        return '%s...%s' % (str[:(maxLen//2)-3], str[-((maxLen//2)-3):])

    def displayAll(self):
        self.showCurrentWord()
        self.displayStatus()
        self.displayLinkList()
        self.displayTrickyList()

    def applyUrlShortcuts(self, command, depth=0):
        words = command.split()
        try:
            numArgs, shortcut = self.settings.urlShortcuts[words[0]]
        except (IndexError, KeyError):
            return command

        for arg in range(numArgs):
            i = arg+1
            try:
                if i == numArgs:
                    # last argument gets all remaining words
                    repl = ' '.join(words[i:])
                else:
                    repl = words[i]
            except IndexError:
                return shortcut

            shortcut = re.sub('%%%d' % i, repl, shortcut)

        shortcut = re.sub('%c', self.itext.url, shortcut)
        shortcut = re.sub('\\$HOME', os.getenv("HOME"), shortcut)
        # apply recursively
        if depth < 10:
            return self.applyUrlShortcuts(shortcut, depth+1)
        else:
            return shortcut

    def go(self, command):
        url = self.applyUrlShortcuts(command)
        self.gotoNewUrl(url)

    def gotoNewUrl(self, url):
        if self.itext:
            self.history.append(self.itext.url)
        self.gotoUrl(url)
        # Turns out to be neater to have the current document in unHistory:
        self.unHistory = [url]
    def goHistory(self, num=1):
        try:
            self.gotoUrl(self.history[-num])
            for i in range(num):
                self.unHistory.append(self.history.pop())
        except IndexError:
            return None
    def goUnHistory(self, num=1):
        try:
            self.gotoUrl(self.unHistory[-num-1])
            for i in range(num):
                self.history.append(self.unHistory.pop())
        except IndexError:
            return None

    def gotoUrl(self, url, noCache=False):
        shownError = False
        try:
            m = re.match('(.*)#(.*)$', url)
            if m:
                baseUrl, anchor = m.groups()
            else:
                baseUrl, anchor = url, None
            cached = self.findCachedDocument(baseUrl)
            if cached and not noCache:
                self.itext = cached
            else:
                self.displayCleanCentred(self.geom.commandline, self.abbreviateStr("Loading %s" % url))
                self.scr.refresh()

                self.interruptible = True
                self.itext = IndexedHypertext(baseUrl, notSentenceEnder=self.notSentenceEnder)
                self.interruptible = False

            if self.itext.lynxErr:
                err = self.itext.lynxErr.strip()
                line = ';'.join(err.splitlines()[0:2])
                self.displayCleanCentred(self.geom.commandline,
                        self.abbreviateStr("Failed: %s" % line))
                shownError = True
                return False

            if anchor:
                self.interruptible = True
                n = self.itext.findAnchor(anchor)
                self.interruptible = False

                if n is not None:
                    self.itext.seekWord(n, setMark=True)
                else:
                    self.displayCleanCentred(self.geom.commandline,
                    self.abbreviateStr("No such anchor: %s" % anchor))
                    shownError = True

            self.displayCleanCentred(self.geom.infoline, self.abbreviateStr("%s" % baseUrl))
            self.displayAll()

            self.cacheDocument(self.itext)

        finally:
            self.interruptible = False
            if not shownError:
                self.clearLine(self.geom.commandline)

    def cacheDocument(self, doc):
        # remove any old cachings of this url:
        for cached in self.cachedDocuments:
            if cached[0] == doc.url:
                self.cachedDocuments.remove(cached)
        self.cachedDocuments.append((doc.url, doc))
        if len(self.cachedDocuments) > self.maxCached:
            self.cachedDocuments.pop(0)
    def findCachedDocument(self, url):
        for (u, doc) in self.cachedDocuments:
            if u == url: return doc
        return None

    def interruptSpeech(self):
        if self.speechsub:
            self.speechsub.kill()
        self.speechsub = None

    def saySentence(self):
        if self.speechsub:
            self.speechsub.wait()
        self.speechsub = None
        i = self.itext.atWord
        quote = self.itext.inQuote()

        s = self.itext.sentenceIndex.nextPos()
        sq = self.itext.startQuoteIndex.nextPos()
        eq = self.itext.endQuoteIndex.nextPos()
        if eq is not None: eq += 1
        stops = [x for x in (s,sq,eq) if x is not None]
        if stops == []: end=None
        else: end = min(stops)

        text=""
        while i != end:
            w = self.itext.getWord(i)
            if not w: break
            text += w + " "
            i+=1
        self.spokeTo = i-1
        self.speechsub = self.sayText(text, quote=quote)
        
    def sayText(self, text, wait=False, quote=False):
        from subprocess import Popen, PIPE
        espeakcmd = (quote and self.sayGenQuotedSpeech or
                self.sayGenSpeech) % (self.wpm)
        try:
            espeakps = Popen( espeakcmd.split(),
                    shell=False, stdin=PIPE, stdout=PIPE, stderr=None)
            espeakps.stdin.write(text.encode("utf-8"))
            espeakps.poll()
            speechps = Popen(self.sayPlaySpeech.split(),
                    stdin=espeakps.stdout, stdout=None, stderr=None,
                    shell=False, close_fds=True)  # these two are important!
            espeakps.stdin.close()
            espeakps.stdout.close()
            return speechps
        except OSError:
            self.displayCleanCentred(self.geom.commandline,
                    "Speech command failed; disabling. Edit "
                    +"sayGenSpeech and sayPlaySpeech in ~/.flinksrc")
            self.speech = False
            return None

    def showCurrentWord(self, word=None):
        if word is None:
            word = self.itext.currentWord()

        tricky = self.isTricky(word)
        linknum = self.itext.currentLink()
        quote = self.itext.inQuote()

        if linknum is None:
            if tricky or (self.boldQuotes and quote):
                attr = curses.A_BOLD
            else: attr = 0
            self.displayCleanCentred(self.geom.wordline, word, attr)
        else:
            self.displayCleanCentred(self.geom.wordline, word,
                    self.attrOfLinknum(linknum) | curses.A_BOLD)

            self.displayLinkListEntry(linknum, True)

            if linknum >= self.linkDisplayEmphNum:
                self.displayLinkListEntry(linknum-self.linkDisplayEmphNum,
                        False)
        
        if self.speech and self.paused:
            self.sayText(word, quote=quote)

        if tricky:
            self.addTricky(self.cleanWord(word))

        if self.showContext and self.paused:
            contextBefore = ''
            contextFrom = self.itext.atWord
            while True:
                w = self.itext.getWord(contextFrom-1)
                if self.itext.atSentenceEnd(contextFrom-1):
                    w += '  '
                if self.itext.atParaEnd(contextFrom-1):
                    w += '  '
                if w and (len(w) + len(contextBefore)
                        + len(word)//2 + 1) < self.geom.maxx//3:
                    contextBefore = w + ' ' + contextBefore
                    contextFrom -= 1
                else:
                    break

            contextAfter = ''
            contextTo = self.itext.atWord
            while True:
                w = ''
                if self.itext.atSentenceEnd(contextTo):
                    w += '  '
                if self.itext.atParaEnd(contextTo):
                    w += '  '
                w += self.itext.getWord(contextTo+1)
                if w and (len(w) + len(contextAfter)
                        + len(word)//2 + 1) < self.geom.maxx//3:
                    contextAfter += ' ' + w
                    contextTo += 1
                else:
                    break

            point = (self.geom.maxx - len(word))//2 - len(contextBefore)
            attr = curses.color_pair(8) | curses.A_BOLD

            addstrwrapped(self.scr, self.geom.wordline, point, contextBefore, attr)
            point = (self.geom.maxx + len(word))//2
            addstrwrapped(self.scr, self.geom.wordline, point, contextAfter, attr)

        if self.showPoint: self.displayStatus()

    def mainLoop(self):
        def sigHandlerQuit(sig, frame):
            raise QuitException
        signal.signal(15, sigHandlerQuit)

        def sigHandlerInt(sig, frame):
            # Occasionally, we can safely allow ^C to interrupt a section of
            # code.
            if self.interruptible:
                raise KeyboardInterrupt
        signal.signal(2, sigHandlerInt)

        def applyCount(method, args):
            # update args, replacing/appending any count argument
            try:
                countarg = self.bindableMethods[method][2]
                if countarg > 0:
                    if len(args) >= countarg:

                        currentArg = args[countarg-1]
                        if (currentArg.__class__ == int and
                                currentArg < 0):
                            self.count = -self.count

                        args[countarg-1] = self.count

                    elif len(args)+1 == countarg:
                        args.append(count)
            except IndexError:
                pass
            endCount()
        def endCount():
            self.counting = False
            self.countWin.erase()
            self.countWin.refresh()
            del self.countWin

        def handleResize():
            self.setGeom()
            self.scr.erase()
            self.scr.refresh()
            self.displayAll()

        self.go(self.initialUrl)
        self.displayStatus()

        self.blankSpace = 0

        def flashWord():
            # handle blanking
            if self.blankSpace > 0:
                self.blankSpace -= 1
                self.displayCleanCentred(self.geom.wordline, "")
                return

            word = self.itext.readWord()

            if word is not None:
                if self.itext.atParaEnd():
                    self.blankSpace = self.paraBlankSpace
                elif self.itext.atSentenceEnd():
                    self.blankSpace = self.sentenceBlankSpace
                else:
                    def isPause(word):
                        try:
                            if word[-1] in "'\"":
                                last = word[-2]
                            else:
                                last = word[-1]
                            return last in ",;:"
                        except IndexError:
                            return False
                    if isPause(word):
                        self.blankSpace = self.pauseBlankSpace

            self.showCurrentWord(word)

            if self.speech and not self.paused and (
                    self.itext.atWord > self.spokeTo
                    or not self.speechsub):
                self.saySentence()

            return

        def handleInput():
            c = None
            escBound = 27 in self.settings.keyBindings
            metaBit = False
            while c != -1:
                c = self.scr.getch()

                if not escBound:
                    if c == 27:
                        metaBit = True
                        continue
                    elif metaBit:
                        if c == -1:
                            c = 27
                        else:
                            # set high bit
                            c |= 0x80
                        metaBit = False

                if c == curses.KEY_RESIZE:
                    handleResize()

                elif self.counting and ord('0') <= c <= ord('9'):
                    self.count = 10*self.count + c - ord('0')
                    self.countWin.addch(chr(c))
                    self.countWin.refresh()

                else:
                    binding = self.settings.keyBindings.get(c)
                    if binding is not None:
                        method = binding[0]
                        args = binding[1][:]

                        if self.counting:
                            applyCount(method, args)

                        self.interruptSpeech()
                        getattr(self, 'doKey'+method).__call__(*args)
                    elif self.counting and c in [27,7]:
                        # esc or ^G: cancel count
                        endCount()

        
        maxStep = 0.2
        ERRTimeout = 0
        class Event:
            def __init__(self, type, time):
                if type not in ["word", "blank"]:
                    raise "BUG: unknown Event type"
                self.type = type
                self.time = time
        nextEvent = Event("word", time())
        if self.blink: nextBlinkTime = time() + self.blinkDelay

        # compensate for blanking at pauses and so on:
        WPMFACTOR = 4.0/5

        # main loop:
        while True:
            try:
                if ERRTimeout > 0:
                    ERRTimeout -= 1
                try:
                    while True:
                        wait = nextEvent.time - time()
                        if wait < 0: break
                        sleep(min(wait, maxStep))
                        beforeInput = time()

                        try:
                            handleInput()
                        except KeyboardInterrupt:
                            pass

                        inputTime = time() - beforeInput
                        nextEvent.time += inputTime
                        if self.blink: nextBlinkTime += inputTime

                    delay = (60.0 * WPMFACTOR) / self.wpm

                    if self.paused or self.itext.atEnd():
                        nextEvent = Event("word",
                                time() + delay)
                        if self.blink:
                            nextBlinkTime = time() + self.blinkDelay
                        continue

                    if nextEvent.type == "word":
                        t = time()
                        if self.blink and nextBlinkTime < time() and (
                                not self.blinkWaitSentence or
                                self.itext.atSentenceEnd()):
                            self.displayCleanCentred(self.geom.wordline,
                                    "{*blink*}")
                            nextBlinkTime = time() + self.blinkDelay
                            waitTime = self.blinkTime
                        else:
                            flashWord()
                            waitTime = delay
                        if self.blankBetween: 
                            nextEvent = Event("blank",
                                    t + waitTime - delay/3)
                        else:
                            nextEvent = Event("word",
                                    t + waitTime)
                    elif nextEvent.type == "blank":
                        self.clearLine(self.geom.wordline)
                        self.scr.refresh()
                        nextEvent = Event("word",
                                time() + delay/3)

                except curses.error:
                    if ERRTimeout == 0:
                        # Curses threw an error, but that might just be because we
                        # need to resize and haven't yet processed KEY_RESIZE.
                        # So we resize now, and see if we get another curses error
                        # within the next couple of goes round the main loop.
                        self.setGeom()
                        ERRTimeout = 2
                    else:
                        raise "Curses returned ERR - terminal too short?"
            except KeyboardInterrupt:
                pass

    def onExit(self):
        # emulating vim - global marks '0'-'9' give positions on last 10 exits
        for i in range(9,0,-1):
            try: self.globalMarks[str(i)] = self.globalMarks[str(i-1)][:]
            except KeyError: pass
        if (self.itext.url and self.itext.url[0] != '!'):
            self.globalMarks['0'] = \
                    [self.itext.url, self.itext.atWord]

    def writeState(self):
        try:
            self.writeMarksFile(self.globalMarks)
        except IOError:
            pass

    bindableMethods = {
            # <name> : [<min args>, <max args>, [<count arg>], [<string args>]]
            'ChangeSpeed' : [1,1,1],
            'SeekSentence' : [0,1,1],
            'SeekParagraph' : [0,1,1],
            'SeekWord' : [0,1,1],
            'SeekLink' : [0,1,1],
            'SeekStart' : [0,0],
            'SeekEnd' : [0,0],
            'History' : [0,1,1],
            'FollowLink' : [1,1,1],
            'Go' : [0,0],
            'GoCurrent' : [0,0],
            'Search' : [0,1,1],
            'SearchAgain' : [0,1,1],
            'Count' : [0,0],
            'Mark' : [0,0],
            'GoMark' : [0,0],
            'Reload' : [0,0],
            'Pause' : [0,0],
            'Command' : [0,0],
            'Skim' : [0,0],
            'SkipLinks' : [0,0],
            'Refresh' : [0,0],
            'Toggle' : [1,1,0,True],
            'Help' : [0,0],
            'Quit' : [0,0],
            }

    def doKeyChangeSpeed(self, amount):
        self.wpm += amount
        if self.wpm < 30: self.wpm = 30
        self.displayStatus()

    def doKeySeekSentence(self, n=1):
        self.itext.seekIndexRel(self.itext.sentenceIndex, n)
        self.displayAll()
    def doKeySeekParagraph(self, n=1):
        self.itext.seekIndexRel(self.itext.paraIndex, n)
        self.displayAll()
    def doKeySeekWord(self, n=1):
        self.itext.seekWordRel(n)
        self.displayAll()
    def doKeySeekLink(self, n=1):
        self.itext.seekIndexRel(self.itext.linkIndex, n)
        self.displayAll()

    def doKeySeekStart(self):
        self.itext.seekWord(0, setMark=True)
        self.displayAll()
    def doKeySeekEnd(self):
        self.itext.seekEnd()
        self.displayAll()

    def doKeyFollowLink(self, n):
        if not self.itext.links: return

        if self.geom.linkDisplayNum > 0:
            n -= 1
            nl = self.itext.linkIndex.at % self.geom.linkDisplayNum
        else:
            nl = self.itext.linkIndex.at

        if n <= nl:
            linknum = self.itext.linkIndex.at + (n-nl)
        else:
            linknum = self.itext.linkIndex.at - self.geom.linkDisplayNum + (n-nl)
        try:
            url = self.itext.links[linknum]["url"]
            self.gotoNewUrl(url)
        except IndexError:
            pass

    def readLine(self, prompt, initial='', shortPrompt=None, history=None,
            getCompletions=None):
        if self.abbreviate or self.geom.maxx <= 10:
            if shortPrompt is not None:
                prompt = shortPrompt
            else:
                prompt = prompt[0]+":"

        promptX = max(1, self.geom.maxx//6 - len(prompt))
        boxX = promptX + len(prompt)
        boxLen = min(self.geom.maxx - boxX - 1, 2*self.geom.maxx//3)

        addstrwrapped(self.scr, self.geom.commandline, promptX,
                prompt[:self.geom.maxx-5])
        self.scr.refresh()

        curses.curs_set(1)

        pad = curses.newpad(1, 200)
        pad.scrollok(1)
        box = TextboxPad(pad,
                self.geom.commandline, boxX,
                self.geom.commandline, boxX + boxLen - 1,
                history=history,
                getCompletions=getCompletions)

        for char in initial:
            box.do_command(char)

        class cancelReadline(Exception):
            pass
        def cancelValidate(ch):
            if ch == ascii.BEL or ch == 27:
                # ^G and escape cancel the readLine
                raise cancelReadline
            return ch
        try:
            try:
                read = box.edit(cancelValidate)
                if history is not None:
                    history.append(read.strip())
                return read
            except cancelReadline:
                return None
        finally:
            curses.curs_set(0)
            self.clearLine(self.geom.commandline)

    def doKeyGo(self):
        def filenameCompletion(partial):
            from glob import glob
            from os.path import isdir

            completions = glob(partial+'*')

            for i in range(len(completions)):
                if isdir(completions[i]):
                    completions[i] += '/'

            return completions

        command = self.readLine("Go: ", history=self.urlHistory,
                getCompletions=filenameCompletion)
        if command:
            self.go(command.strip())

    def doKeyGoCurrent(self):
        command = self.readLine("Go: ", self.itext.url, history=self.urlHistory)
        if command:
            self.go(command.strip())

    def charOfSearchDir(self, dir=1):
        if dir == 1: return '/'
        else: return '?'
    def search(self, pattern, n=1):
        dir = _cmp(n,0)
        self.displayCleanCentred(self.geom.commandline,
                self.charOfSearchDir(dir)+pattern)
        try:
            self.interruptible = True
            for i in range(abs(n)):
                ret = self.itext.search(pattern.strip(), dir)
                if not ret: break
            self.displayAll()
        finally:
            self.interruptible = False
            self.clearLine(self.geom.commandline)
    def doKeySearch(self, n=1):
        dir = _cmp(n,0)
        pattern = self.readLine("Search: ",
            shortPrompt = self.charOfSearchDir(dir),
            history=self.searchHistory)
        if pattern:
            self.search(pattern, n)
            self.lastSearch = {"pattern": pattern, "dir": dir}
    def doKeySearchAgain(self, n=1):
        if self.lastSearch:
            self.search(self.lastSearch["pattern"],
                    self.lastSearch["dir"]*n)

    def doKeyPause(self):
        self.paused = not self.paused
        self.displayStatus()
        if self.showContext:
            self.showCurrentWord()

    def doKeyHistory(self, n=-1):
        if n < 0:
            self.goHistory(-n)
        else:
            self.goUnHistory(n)

    def doKeyCount(self):
        self.counting = True
        self.count = 0

        boxX = self.geom.maxx//6
        boxLen = min(self.geom.maxx - boxX - 1, 2*self.geom.maxx//3)
        self.countWin = curses.newwin(1, boxLen, self.geom.commandline, boxX)
        if not self.abbreviate:
            prompt = "Count: "
        else:
            prompt = "C"
        addstrwrapped(self.countWin, None, None, prompt[:self.geom.maxx])
        self.countWin.refresh()

    def doKeyMark(self):
        key = self.blockGetKey(self.geom.commandline,
                self.abbreviate and "m" or "Set mark:")
        if key is None or len(key) != 1:
            return
        if 'a' <= key <= 'z':
            # local mark
            self.itext.mark(key)
        elif 'A' <= key <= 'Z':
            # global marks
            self.globalMarks[key] =\
                [self.itext.url, self.itext.atWord]
    def doKeyGoMark(self):
        key = self.blockGetKey(self.geom.commandline,
                self.abbreviate and "'" or "Go to mark:")
        if key is None or len(key) != 1:
            return
        if 'a' <= key <= 'z' or key == '\'':
            # local mark
            self.itext.goMark(key)
            self.displayAll()
        elif key in self.globalMarks:
            # global marks
            url, pos = self.globalMarks[key]
            if url != self.itext.url:
                self.gotoNewUrl(url)
            self.itext.seekWord(pos, setMark=True)
            self.displayAll()

    def doKeyReload(self):
        n = self.itext.atWord
        self.gotoUrl(self.itext.url, True)
        self.itext.seekWord(n)
        self.displayAll()

    def doKeyCommand(self):
        def handleErr(err):
            self.displayCleanCentred(self.geom.commandline, err)
        command = self.readLine(':', shortPrompt=':',
                history=self.commandHistory)
        if command:
            self.settings.processCommand(command, handleErr)
        self.displayAll()

    def doKeySkim(self):
        if not self.skim:
            self.normalWpm = self.wpm
            self.wpm = self.skimWpm
            self.skim = True
        else:
            self.skimWpm = self.wpm
            self.wpm = self.normalWpm
            self.skim = False
        self.displayAll()

    def doKeySkipLinks(self):
        sInd = self.itext.sentenceIndex
        lInd = self.itext.linkIndex
        si = sInd.at
        ps = sInd[si]
        ns = sInd[si+1]
        while ns is not None and lInd[lInd.findIndex(ns-1)] >= ps:
            si+=1
            ps = ns
            ns = sInd[si+1]
        if ns:
            self.itext.seekWord(ns, setMark=True)
            self.displayAll()

    def doKeyRefresh(self):
        self.scr.redrawwin()

    def doKeyToggle(self, var):
        if hasattr(self, var) and type(getattr(self,var)) == bool:
            setattr(self, var, not getattr(self,var))

    def doKeyHelp(self):
        self.go("special:README")

    def doKeyQuit(self):
        raise QuitException
