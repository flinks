# Part of flinks
# (C) Martin Bays 2008
# Released under the terms of the GPLv3

from __future__ import unicode_literals

import sys, os

from string import *

import re
from io import BytesIO, StringIO
from select import select

from .lynxDump import lynxDump

from .constants import USER_AGENT

def placeInList(ref, list, hint=0):
    if list == []: return -1

    lowerBound = max(0, hint)
    while lowerBound > 0 and ref < list[lowerBound]:
        lowerBound -= 1

    for i,e in enumerate( list[lowerBound:] ):
        if ref < e:
            return lowerBound + i - 1
    return lowerBound + i

def getItemCapped(list, i):
    if i < 0: i = 0
    if i >= len(list): i = len(list)-1
    return list[i]


STATE_WHITESPACE = 0
STATE_NEWLINE = 1
STATE_WORD = 2
STATE_LINKNUM = 3
STATE_HYPHEN = 4

class BlockException(BaseException): pass

class IndexedHypertext:
    def __init__(self, url, lynxArgs=[], forcehtml=False, notSentenceEnder=""):
        class Index:
            """An increasing list of points (word numbers) in the text. 'at'
            is maintained as the index of the last indexed point before the
            current word position."""
            def __init__(iself):
                iself.list = []
                iself.at = -1
            def findIndex(iself, ref, near=False):
                """Returns last index for which list[index] <= ref, returning
                -1 if none.
                """
                if iself.list == []: return -1

                if near:
                    i = max(0, iself.at)
                else:
                    # binary search
                    i = len(iself)//2
                    di = i//2 + i%2
                    while di > 1:
                        n = iself[i]
                        i += di * (-1)**(n is None or n <= ref)
                        i = max(0,i)
                        di = di//2 + di%2

                while i > 0 and (iself[i] is None or ref < iself[i]):
                    i -= 1
                while True:
                    next = iself[i+1]
                    if next is None or ref < iself[i+1]:
                        break
                    i += 1
                return i

            def setPos(iself, to, near=False):
                iself.at = iself.findIndex(to, near)
            def pos(iself):
                return iself[iself.at]
            def nextPos(iself):
                return iself[iself.at+1]
            def append(iself, v):
                if iself.list == [] or iself.list[-1] != v:
                    iself.list.append(v)
            def __getitem__(iself, n):
                if n < 0:
                    return -1
                try:
                    while len(iself.list) <= n:
                        self.parseMore()
                    return iself.list[n]
                except (StopIteration, BlockException):
                    return None
            def __len__(iself):
                return len(iself.list)

        self.url = url
        self.notSentenceEnder=notSentenceEnder

        self.text = None
        self.lynxErr = ""
        self.links = None
        if (url and url[0] == '!'):
            # command
            from subprocess import Popen, PIPE
            sub=Popen(url[1:], shell=True, stdin=PIPE, stdout=PIPE, 
                    stderr=PIPE, bufsize=0, universal_newlines=True)
            sub.stdin.close()
            self.text = sub.stdout
        if not (forcehtml or re.match('.*\.html?$', url)):
            # first try opening as a local plaintext file
            try: self.text = open(os.path.expanduser(url), 'r', 0)
            except IOError: pass
        if not self.text:
            dumped, linkUrls, self.lynxErr = lynxDump(url, lynxArgs=lynxArgs)
            self.text = StringIO(dumped)
            self.links = [ {"url": url, "word": ''} for url in linkUrls ]

        self.knownAnchors = {}

        self.words = []

        self.marks = {}

        # index word position of start of each sentence, paragraph, link
        self.sentenceIndex = Index()
        self.sentenceIndex.append(0)
        self.paraIndex = Index()
        self.paraIndex.append(0)
        self.linkIndex = Index()
        self.startQuoteIndex = Index()
        self.endQuoteIndex = Index()
        self.indices = ( self.sentenceIndex, self.paraIndex, self.linkIndex,
                self.startQuoteIndex, self.endQuoteIndex )

        self.parserGenerator = self.getParserGenerator()
        self.parsed = False
        self.atWord = -1
        self.nextWord = 0
        if len(self.linkIndex) and self.linkIndex[0] == 0:
            self.linkIndex.at = 0

    def readWouldBlock(self):
        try: return select([self.text.fileno()],[],[],0)[0] == []
        except:
            # select will raise an exception on some platforms (e.g. windows).
            # Just assume non-blocking.
            return False

    def parseMore(self):
        if next(self.parserGenerator) is None:
            raise BlockException

    def getParserGenerator(self):
        class Parser:
            def __init__(parseSelf):
                parseSelf.wordNum = 0
                parseSelf.maybeLink = False
                parseSelf.inQuote = False
            def endsSentence(parseSelf, word):
                if word and word[-1] == '"': word = word[:-1]
                if word and word[-1] in ".!?":
                    return not re.match(self.notSentenceEnder, word)
                return False
                
            def wordCompleted(parseSelf, word):
                try:
                    if parseSelf.endsSentence(word):
                        self.sentenceIndex.append(parseSelf.wordNum+1)

                except IndexError:
                    pass

                def handleSubword(word, linking):
                    if linking:
                        self.linkIndex.append(parseSelf.wordNum)
                        self.links[len(self.linkIndex)-1]["word"] = word
                    self.words.append(word)
                    parseSelf.wordNum += 1
                    return word
                linking = False
                if parseSelf.maybeLink:
                    parseSelf.maybeLink = False
                    def regexp():
                        return ('(.*?)' +
                                '\[' + repr(len(self.linkIndex)+1+linking) + '\]' +
                                '(.*)$')
                    while True:
                        m = re.match(regexp(), word)
                        if m:
                            wordstart, wordend = m.groups()
                            if wordstart:
                                yield handleSubword(wordstart, linking)
                            if ( self.links and
                                    len(self.linkIndex) < len(self.links) ):
                                linking = True
                            word=wordend
                            if word=="":
                                word="{}"
                        else: break

                yield handleSubword(word, linking)
            
            def paraEnded(parseSelf):
                self.paraIndex.append(parseSelf.wordNum)
                if not (self.sentenceIndex and
                        self.sentenceIndex[-1] == parseSelf.wordNum):
                    self.sentenceIndex.append(parseSelf.wordNum)
                if parseSelf.inQuote:
                    self.endQuoteIndex.append(parseSelf.wordNum)
                    parseSelf.inQuote = False

            def quoteMark(parseSelf):
                if parseSelf.inQuote:
                    self.endQuoteIndex.append(parseSelf.wordNum)
                else:
                    self.startQuoteIndex.append(parseSelf.wordNum)
                parseSelf.inQuote = not parseSelf.inQuote

            def gen(parseSelf):
                state = STATE_WHITESPACE
                wordacc = ''
                while True:
                    while self.readWouldBlock():
                        yield None

                    c = self.text.read(1)

                    if c == '': break

                    if state in [STATE_WHITESPACE,STATE_NEWLINE]:
                        if c == '\n':
                            if state == STATE_NEWLINE:
                                parseSelf.paraEnded()
                            state = STATE_NEWLINE
                        elif c in whitespace:
                            state = STATE_WHITESPACE
                        else:
                            state = STATE_WORD

                            wordacc = c
                            if c == '[':
                                parseSelf.maybeLink = True
                            if c == '"':
                                parseSelf.quoteMark()


                    elif state == STATE_WORD:
                        if c in whitespace+'\"':
                            if c == '\n': state = STATE_NEWLINE
                            else: state = STATE_WHITESPACE
                            if c == '"':
                                for w in parseSelf.wordCompleted(wordacc+'\"'):
                                    yield w
                                parseSelf.quoteMark()
                            else:
                                for w in parseSelf.wordCompleted(wordacc):
                                    yield w
                            wordacc=''
                        elif c == '-':
                            state = STATE_HYPHEN
                        else:
                            wordacc += c
                            if c == '[':
                                parseSelf.maybeLink = True
                    elif state == STATE_HYPHEN:
                        if c == '-':
                            if wordacc:
                                for w in parseSelf.wordCompleted(wordacc):
                                    yield w
                                wordacc=''
                            for w in parseSelf.wordCompleted('--'):
                                yield w
                            state = STATE_WHITESPACE
                        elif c in whitespace:
                            wordacc += '-'
                            for w in parseSelf.wordCompleted(wordacc):
                                yield w
                            wordacc=''
                            state = STATE_WHITESPACE
                        else:
                            wordacc += '-'
                            if len(wordacc) > 4 or c == '[':
                                for w in parseSelf.wordCompleted(wordacc):
                                    yield w
                                wordacc=''
                            wordacc += c
                            state = STATE_WORD
                            if c == '[':
                                parseSelf.maybeLink = True
                            if c == '"':
                                parseSelf.quoteMark()

                if wordacc:
                    for w in parseSelf.wordCompleted(wordacc):
                        yield w
                self.parsed = True

        parser = Parser()
        return parser.gen()

    def getWord(self, n):
        if n < 0: return ''
        try:
            return self.words[n]
        except IndexError:
            try:
                for i in range(n - len(self.words) + 1):
                    self.parseMore()
            except (StopIteration, BlockException):
                return ''
            return self.words[n]

    def currentLink(self):
        try:
            if self.atWord == self.linkIndex.pos():
                return self.linkIndex.at
        except IndexError: pass
        return None

    def readWord(self):
        word = self.getWord(self.nextWord)
        if word == '':
            return None

        self.atWord = self.nextWord
        self.nextWord += 1

        for index in self.indices:
            index.setPos(self.atWord, True)

        return word

    def currentWord(self):
        if self.atWord == -1:
            return ''
        else:
            return self.getWord(self.atWord)

    def isEmpty(self):
        return self.getWord(0) == ''
    def atEnd(self):
        if self.atWord == len(self.words) - 1:
            try:
                self.parseMore()
                return False
            except (StopIteration, BlockException):
                return True

    def seekWord(self, toWord, setMark=False):
        if setMark:
            self.mark('\'')
        # parse to the point, and don't go beyond the end:
        if self.getWord(toWord) == '':
            toWord = len(self.words)-1

        near = abs(self.atWord - toWord) < 100
        self.atWord = toWord
        self.nextWord = max(0,toWord)

        for index in self.indices:
            index.setPos(toWord,near)

    def seekWordRel(self, rel):
        self.seekWord(max(0, self.atWord+rel))

    def parseAll(self):
        try:
            while True:
                self.parseMore()
        except (StopIteration, BlockException):
            pass

    def linkOfWord(self, n):
        i = placeInList(n, self.linkIndex)
        if i != -1 and n == self.linkIndex[i]:
            return i
        else:
            return None

    def seekEnd(self):
        self.parseAll()
        self.seekWord(len(self.words)-1, setMark=True)

    def seekIndex(self, index, n):
        if n < 0: n = 0
        i = index[n]
        if i is not None:
            self.seekWord(i)
        elif len(index) != 0:
            self.seekWord(index[-1])
    def seekIndexRel(self, index, rel):
        self.seekIndex(index, index.at+rel)

    def atSentenceStart(self, n=None):
        if n == None:
            return self.atWord == self.sentenceIndex.pos()
        else:
            i = self.sentenceIndex.findIndex(n, True)
            return i != -1 and self.sentenceIndex[i] == n
    def atParaStart(self, n=None):
        if n == None:
            return self.atWord == self.paraIndex.pos()
        else:
            i = self.paraIndex.findIndex(n, True)
            return i != -1 and self.paraIndex[i] == n
    def atSentenceEnd(self, n=None):
        if n == None:
            return self.atWord + 1 == self.sentenceIndex.nextPos()
        else:
            return self.atSentenceStart(n+1)
    def atParaEnd(self, n=None):
        if n == None:
            return self.atWord + 1 == self.paraIndex.nextPos()
        else:
            return self.atParaStart(n+1)

    def inQuote(self, point=None):
        if not point:
            point = self.atWord
        sq = self.startQuoteIndex.findIndex(point, True)
        eq = self.endQuoteIndex.findIndex(point, True)
        return ( (sq>=0 and not eq>=0) or (sq>=0 and self.startQuoteIndex[sq]
                >= self.endQuoteIndex[eq]) )

    def search(self, pattern, dir=1, matchCase=False, wrap=False):

        patternWords = pattern.split()

        if len(patternWords) == 0:
            return

        if dir == 1:
            initial = self.atWord+1
        else:
            initial = self.atWord-1

        i = initial

        while True:
            if i < 0:
                if wrap:
                    self.parseAll()
                    i = len(self.words) - 1
                else:
                    return False
            matched = 0
            for pi in range(len(patternWords)):
                word = self.getWord(i+pi)
                if word == '' and dir == 1:
                    # reached end
                    if wrap:
                        i = -1
                        break
                    else:
                        return False
                pword = patternWords[pi]
                if matchCase:
                    f = word.find(pword)
                else:
                    f = word.lower().find(pword.lower())
                if (f == -1 or
                        (pi > 0 and f > 0) or
                        (pi == 0 and len(patternWords) > 1 and
                            len(word) - f != len(pword) # doesn't match to end
                            )):
                    break
                else:
                    matched += 1
                if matched == len(patternWords):
                    self.seekWord(i, setMark=True)
                    return True
            if dir == 1:
                i += 1
            else:
                i -= 1
            if wrap and i == initial:
                return False

    def mark(self, char):
        self.marks[char] = self.atWord
    def goMark(self, char):
        if char in self.marks:
            n = self.marks[char]
            self.seekWord(n, setMark=True)
    def findAnchor(self, anchor):
        """findAnchor: return index of a named anchor, or None on failure.

        XXX: the implementation here is rather hacky. It is unlikely to be
        very robust.

        We retrieve a copy of the html, search and replace to insert a marker
        at an appropriate point, then process the result via lynx and search
        for the marker."""

        anchor = anchor.lower()

        try:
            return self.knownAnchors[anchor]
        except KeyError:
            if not hasattr(self, "anchorTempIndexed"):
                from tempfile import mkstemp

                try: from urllib import URLopener
                except ImportError:
                    from urllib.request import URLopener

                class myURLopener(URLopener):
                    version = USER_AGENT

                (_, tempname) = mkstemp()
                try:
                    try:
                        # fetch a copy - note that urllib keeps a cache, so this only
                        # actually retrieves the url the first time.
                        opener = myURLopener()
                        opener.retrieve(self.url, tempname)

                        temphtml = open(tempname).read()

                        def getRep(m):
                            tag = m.group(0)
                            element, attribute, value = m.groups()

                            # don't want to pick up meta elements, since they
                            # are in the header. Of course this is rather
                            # hacky.
                            if element == "meta":
                                return tag

                            return '%s__FLINKS_ANCHOR_%s__' % (tag, value)

                        temphtml = re.sub(
                                '<\s*(\w+)\s[^>]*?(name|id)\s*=\s*"?([\w-]+)"?[^>]*>',
                                getRep, temphtml)
                        open(tempname, 'w').write(temphtml)

                        self.anchorTempIndexed = IndexedHypertext(tempname,
                                lynxArgs=["-force-html"])
                        self.anchorFakeWordCount = 0
                    except (IOError, UnicodeDecodeError):
                        return None
                finally:
                    os.remove(tempname)

            while True:
                if not self.anchorTempIndexed.search('__FLINKS_ANCHOR_'):
                    return None

                m = re.search('__FLINKS_ANCHOR_([\w-]+)__',
                        self.anchorTempIndexed.currentWord())
                if m:
                    name = m.group(1).lower()
                    n = self.anchorTempIndexed.atWord - self.anchorFakeWordCount
                    whole = re.match('^__FLINKS_ANCHOR_([\w-]+)__$',
                        self.anchorTempIndexed.currentWord())
                    if whole:
                        self.anchorFakeWordCount += 1
                    self.knownAnchors[name] = n
                    if name == anchor:
                        return n
                else:
                    continue
