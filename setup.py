#!/usr/bin/env python

from distutils.core import setup

setup(
	scripts=['flinks'],
	packages=['flinkspkg'],

	# Metadata:
	name="flinks",
	version="0.5.0",
	description="flinks",
	long_description="flashing word browser",
	author="Martin Bays",
	author_email="mbays@sdf.lonestar.org",
	url="http://mbays.freeshell.org/flinks/",
	download_url="http://mbays.freeshell.org/flinks/",
	classifiers = [
	    'Development Status :: 3 - Beta',
	    'Environment :: Console',
	    'Intended Audience :: End Users/Desktop',
	    'License :: OSI Approved :: GNU General Public License (GPL)',
	    'Programming Language :: Python',
	    'Topic :: Internet :: WWW/HTTP :: Browsers',
	],
    )
